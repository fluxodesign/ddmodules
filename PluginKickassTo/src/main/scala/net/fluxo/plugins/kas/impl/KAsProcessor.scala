package net.fluxo.plugins.kas.impl

import java.io._
import java.net.{MalformedURLException, Socket, URLEncoder}
import java.util

import com.google.gson.Gson
import net.fluxo.plugins.kas.dbo.{KASObject, KASPage, KAsDetails}
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.log4j.{Level, Logger}
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.select.Elements

/**
 * This class deals with searching and interpretation of the results of said searches of Kickass.to.
 * <p>Part of the DownloadDaemon plugin framework.</p>
 *
 * @author Ronald Kurniawan (viper)
 * @version 0.1, 6/01/15.
 */
class KAsProcessor {

    private var _mailLogger = "none"
    private val _url_1 = "kat.cr"
	private val _url_2 = "kickass.hd-torrent.net"
	private var _urlToUse = ""
    private final val _httpUrl = "https://[site-url]"
    private final val _searchUrl = "https://[site-url]/usearch/[term][cat]/"
    private final val _searchUrlPage = "https://[site-url]/usearch/[term][cat]/[page]/"

    /**
     * Set the name of our mail logger.
     * @param name name of the mail logger
     */
    def setMailLogger(name: String): Unit = {
        _mailLogger = name
    }

    /**
     * Enumeration object containing the categories from Kickass.to
     */
    object KASCats extends Enumeration {
        type Cat = Value
        val All = Value("")
        val Movies = Value("movies")
        val TV = Value("tv")
        val Anime = Value("anime")
        val Music = Value("music")
        val Books = Value("books")
        val Games = Value("games")
        val Apps = Value("applications")
        val XXX = Value("xxx")
        def isValidCat(s: String) = values.exists(_ equals s)
    }

	private def getSiteToUse() {
		if (isSiteAlive(_url_1)) {
			_urlToUse = _url_1
		} else if (isSiteAlive(_url_2)) {
			_urlToUse = _url_2
		}
	}

    /**
     * Check whether our torrent site is alive.
     *
     * @return true if site is reachable; false otherwise
     */
    def isSiteAlive(url:String): Boolean = {
        var reachable: Boolean = false
        var socket: Socket = null
        try {
            socket = new Socket(url, 80)
            reachable = true
            if (socket != null) socket close()
        } catch {
            case ioe: IOException =>
                writeLog("Failed to close socket connection to Kat.cr", Level.ERROR)
        }
        reachable
    }

    /**
     * Send a query requesting the details of a bittorrent object based on a URL.
     *
     * @param url URL pointing to the details of the bittorrent
     * @return a JSON representation of <code>net.fluxo.dd.dbo.TPBDetails</code>
     */
    def queryDetails(url: String): String = {
        val sb = new StringBuilder
	    var count = 0
	    while ((_urlToUse length) == 0 && count < 10) {
		    getSiteToUse()
		    count += 1
	    }
	    writeLog("Using TORRENT site: " + _urlToUse, Level.INFO)
	    if (_urlToUse.length > 0) {
		    val response = crawlServer(url)
		    val document = Jsoup parse response
		    val kd = new KAsDetails
		    kd.Request_:(url)
		    kd.Uploaded_:(parseDetails(document, searchForUploaded = true))
		    kd.Info_:(parseDetails(document, searchForUploaded = false))
		    val gson = new Gson()
		    sb append (gson toJson kd)
	    }
        sb toString()
    }

    /**
     * Send a query request to the site, filtered by page number and a category.
     *
     * @param searchTerm search term
     * @param page page number to display (starting from 0)
     * @param cat array of categories to supply to the search
     * @return a JSON representation of <code>net.fluxo.dd.dbo.KASPage</code>
     */
    def query(searchTerm: String, page: Int, cat: String): String = {
        val sb = new StringBuilder
        var request = _searchUrl
	    var count = 0
        val encodedSearchTerm = URLEncoder encode (searchTerm, "UTF-8")
	    while (_urlToUse.length == 0 && count < 10) {
		    getSiteToUse()
		    count += 1
	    }
	    writeLog("Using TORRENT site: " + _urlToUse, Level.INFO)
	    if (_urlToUse.length > 0) {
		    request = request replaceAllLiterally("[site-url]", _urlToUse)
		    request = request replaceAllLiterally("[term]", encodedSearchTerm)
		    request = request replaceAllLiterally("[page]", page.toString)
		    val kCat = new StringBuilder
		    kCat append "%20category:" append cat
		    request = request replaceAllLiterally("[cat]", kCat toString())
		    // make sure that kickass is active and hand it over to jsoup
		    val response = crawlServer(request)
		    val document = Jsoup parse response
		    val totalItems = queryTotalItemsFound(document)
		    val itemList = parseItems(document)
		    val kasPage = new KASPage
		    kasPage.TotalItems_:(totalItems)
		    kasPage.KASItems_:(itemList)
		    val gson = new Gson()
		    sb append (gson toJson kasPage)
	    }
        sb toString()
    }

    /**
     * Search for the total items string inside the jsoup's <code>Document</code> object.
     *
     * @param doc a <code>org.jsoup.nodes.Document</code> object
     * @return a number of total items found in this search
     */
    def queryTotalItemsFound(doc: Document): Int = {
        var ret: Int = 0
        val span: Elements = doc getElementsByTag "span"
        val iterator = span iterator()
        while (iterator.hasNext) {
            var strItemsFound = iterator next() text()
            if ((strItemsFound indexOf "results") > -1 && (strItemsFound indexOf "from") > -1) {
                strItemsFound = strItemsFound substring(strItemsFound indexOf "from")
                strItemsFound = (strItemsFound replaceAll("\\D", "")).trim
                try {
                    ret = strItemsFound.toInt
                } catch {
                    case nfe: NumberFormatException =>
                }
            }
        }
        ret
    }

    /**
     * Return the details of the bittorent object, scraped from the <code>Document</code> object.
     *
     * @param doc a <code>org.jsoup.nodes.Document</code> object
     * @param searchForUploaded whether we are looking for "uploaded" value or "details"
     * @return the details of the bittorent object
     */
    def parseDetails(doc: Document, searchForUploaded: Boolean): String = {
        var nfo = ""
        val div: Elements = doc select "div[class]"
        val iterator = div iterator()
        while (iterator.hasNext) {
            val d = iterator.next
            if (searchForUploaded) {
                var text = d.text
                if ((text indexOf "Added on") > -1) {
                    text = text replace ("Added on", "")
                    if ((text indexOf "by") > -1) {
                        text = text substring (0, text.indexOf("by"))
                        text = text.trim
                        if (text.length < 15) nfo = text
                    }
                }
            } else {
                if ((d attr "class" equals "textcontent") && (d attr "id" equals "desc")) {
                    nfo = d.text
                }
            }
        }
        nfo
    }

    /**
     * Process a <code>Document</code> object to parse <code>TPBObject</code> items.
     *
     * @param doc a <code>org.jsoup.nodes.Document</code> object
     * @return a <code>java.util.ArrayList</code> containing TPBObjects
     */
    def parseItems(doc: Document): util.ArrayList[KASObject] = {
        val list = new util.ArrayList[KASObject]
        val tr: Elements = doc getElementsByTag "tr"
        val trIterator = tr.iterator()
        while (trIterator.hasNext) {
            val oneTR = trIterator.next
            if (((oneTR attr "class" equals "odd") || (oneTR attr "class" equals "even")) && (oneTR attr "id" startsWith "torrent_")) {
                val t = new KASObject
                val oneTD = oneTR child 0
                val div0 = oneTD child 0
                // one of these anchors has magnet URL, while the other has torrent URL
                val anchorIterator = div0 children() iterator()
                while (anchorIterator.hasNext) {
                    val anchor = anchorIterator.next
                    if (anchor attr "title" equals "Torrent magnet link") {
                        t.MagnetURL_:(anchor attr "href")
                    } else if (anchor attr "title" equalsIgnoreCase "Download torrent file") {
	                    var turl = anchor attr "href"
	                    //turl = turl substring(0, turl indexOf "?")
	                    // the downloaded .torrent file is a gz-compressed file, so
	                    // when it is time to download the URL, don't forget to rename
	                    // the resulting file as such...
                        t.TorrentURL_=(turl)
                    }
                }
                val div1Iterator = oneTD child 1 children() iterator()
                while (div1Iterator.hasNext) {
                    val element = div1Iterator.next
                    if ((element.tagName equals "a") && ((element attr "class" indexOf "torType") > -1)) {
                        t.DetailsURL_=("https://kat.cr" + (element attr "href"))
                    } else if (element.tagName equals "div") {
                        val div2Iterator = element children() iterator()
                        while (div2Iterator.hasNext) {
                            val obj = div2Iterator.next
                            if (obj attr "class" equals "cellMainLink") {
                                t.Title_:(obj.text)
                            } else if (obj.tagName.equals("span") && (obj attr "class" equals "font11px lightgrey block")) {
                                var uploader = obj.text
                                uploader = uploader replace("Posted by ", "")
                                if ((uploader indexOf "in") > -1) {
                                    uploader = uploader.substring(0, uploader.indexOf("in"))
                                }
                                t.Uploader_=(uploader)
                                var category = obj.text
                                if ((category indexOf "in") > -1) {
                                    category = category.substring(category.indexOf("in")+2).trim
                                    t.Type_:(category)
                                }
                            }
                        }
                    }
                }
                val twoTD = oneTR child 1
                t.Size_=(twoTD.text)
                val fourTD = oneTR child 3
                t.Age_=(fourTD.text)
                val seeder = oneTR child 4
                t.Seeders_:(Integer parseInt seeder.text)
                val leecher = oneTR child 5
                t.Leechers_:(Integer parseInt leecher.text)
                list add t
            }
        }
        list
    }

    /**
     * Contact a web server and request its resource.
     *
     * @param request a string containing the target URL
     * @return response from the server
     */
    private def crawlServer(request: String): String = {
        val response = new StringBuilder
        try {
            val htClient = HttpClientBuilder.create().build()
            val htGet = new HttpGet(request)
            htGet addHeader ("Content-Type", "text/html; charset=UTF-8")
            htGet addHeader("User-Agent", "FluxoAgent/0.1")
            val htResponse = htClient execute htGet
            val br = new BufferedReader(new InputStreamReader(htResponse.getEntity.getContent))
            var line = br.readLine
            while (line != null) {
                response append line
                line = br.readLine
            }
            br close()
            htClient close()
        } catch {
            case mue: MalformedURLException =>
                writeLog("URL " + (request toString()) + " is malformed", Level.ERROR)
                writeLog(stackTraceToString(mue), Level.ERROR)
            case ioe: IOException =>
                writeLog("IO/E: " + ioe.getMessage, Level.ERROR)
                writeLog(stackTraceToString(ioe), Level.ERROR)
            case e: Exception =>
                writeLog("CrawlServer Exception: " + e.getMessage, Level.ERROR)
                writeLog(stackTraceToString(e), Level.ERROR)
        }
        response toString()
    }

    /**
     * Writes a message into the log file, and depending on the Log level, should trigger a Mail response
     * @param entry message to write into the Log
     * @param logLevel <code>org.apache.log4j.Level</code> object
     */
    def writeLog(entry: String, logLevel: Level) {
        val rootLogger = Logger.getRootLogger
        var mailLogger: Option[Logger] = None
        if (!(_mailLogger equals "none")) mailLogger = Some(Logger getLogger _mailLogger)
        val isMailAppenderActive = {
            if (!mailLogger.isDefined) false
            else {
                mailLogger.orNull.getLevel != Level.OFF
            }
        }
        logLevel match {
            case Level.WARN =>
                rootLogger warn entry
                if (mailLogger.isDefined && isMailAppenderActive) (mailLogger orNull) warn entry
            case Level.TRACE =>
                rootLogger trace entry
                if (mailLogger.isDefined && isMailAppenderActive) (mailLogger orNull) trace entry
            case Level.INFO =>
                rootLogger info entry
            case Level.FATAL =>
                rootLogger fatal entry
                if (mailLogger.isDefined && isMailAppenderActive) (mailLogger orNull) fatal entry
            case Level.ERROR =>
                rootLogger error entry
                if (mailLogger.isDefined && isMailAppenderActive) (mailLogger orNull) error entry
            case Level.DEBUG =>
                rootLogger debug entry
        }
    }

    /**
     * Turns an exception stack trace into a <code>String</code>.
     *
     * @param e Exception
     * @return a string containing the stack trace of the Exception
     */
    def stackTraceToString(e: Throwable): String = {
        val sw = new StringWriter
        val pw = new PrintWriter(sw)
        e printStackTrace pw
        sw.toString
    }
}

/**
 * A singleton object of KAsProcessor
 */
object KASP extends KAsProcessor
