package net.fluxo.plugins.kas.dbo

/**
 * Data Object for representing details of a bittorent object from Kickass
 * torrents site.
 *
 * @author Ronald Kurniawan (viper)
 * @version 0.1, 25/01/15
 */
class KAsDetails {

	private val _id = "KAS_DETAILS"

	private var _info: String = ""

	def Info: String = { _info }
	def Info_:(value: String) { _info = value }

	private var _uploaded: String = ""

	def Uploaded: String = _uploaded
	def Uploaded_:(value: String) { _uploaded = value}

	private var _request: String = ""

	def Request: String = _request
	def Request_:(value: String) = { _request = value }
}
