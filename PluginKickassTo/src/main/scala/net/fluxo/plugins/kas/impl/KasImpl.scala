package net.fluxo.plugins.kas.impl

import java.net.URLDecoder

import net.fluxo.plugins.kas.TrKas
import net.xeoh.plugins.base.annotations.events.PluginLoaded
import net.xeoh.plugins.base.annotations.meta.Author
import net.xeoh.plugins.base.annotations.{Capabilities, PluginImplementation}
import org.apache.commons.codec.net.URLCodec
import org.apache.log4j.Level

/**
 * @author Ronald Kurniawan (viper)
 * @version 0.1, 6/01/15.
 */
@PluginImplementation
@Author(name = "Ronald Kurniawan (viper)")
class KasImpl extends TrKas {

    private val _command = "KAST"

    override def primaryCommand(): String = _command

    @Capabilities
    def capabilities(): Array[String] = {
        Array("targetSite:Kat.cr")
    }

    @PluginLoaded
    def pluginDetected(): Unit = {
        writeToLog("Plugin loaded: PluginKickassTo", Level.INFO)
    }

    override def setMailLoggerName(name: String) {
        KASP setMailLogger name
    }

    override def writeToLog(entry: String, logLevel: Level) {
        KASP writeLog(entry, logLevel)
    }

    override def process(fullCommand: Array[String]): String = {
        var retVal = "ERROR"
        fullCommand(1) match {
            case "KAST" =>
                if (fullCommand.length < 3) retVal = "ERR LENGTH"
                else {
                    if (fullCommand.length == 3 && !fullCommand(2).startsWith("ST=")) retVal = "SYNTAX ERROR 1"
                    else if (fullCommand.length == 4 && (!fullCommand(2).startsWith("ST=") || !fullCommand(3).startsWith("PG="))) retVal = "SYNTAX ERROR 2 "
                    else if (fullCommand.length >= 5 && (!fullCommand(2).startsWith("ST=") || !fullCommand(3).startsWith("PG=") || !fullCommand(4).startsWith("CAT="))) retVal = "SYNTAX ERROR 3"
                    else {
                        val searchTerm: String = {
                            val ucodec = new URLCodec
                            ucodec decode fullCommand(2).substring("ST=".length) replaceAllLiterally("\"", "")
                        }
                        val page: Int = {
                            if (fullCommand.length >= 4) {
                                try {
                                    fullCommand(3).substring("PG=".length).toInt
                                } catch {
                                    case nfe: NumberFormatException => 0
                                }
                            } else 0
                        }
                        val cat: String = {
                            fullCommand(4).substring("CAT=".length) replaceAllLiterally("\"", "")
                        }

                        retVal = KASP query(searchTerm, page, cat)
                    }
                }
			case "KASTDETAILS" =>
                if (fullCommand.length != 3) "ERR_KICKASS_DETAILS"
                else {
                    val detailsURL = URLDecoder decode(fullCommand(2), "UTF-8")
                    if (!(detailsURL startsWith "https://kat.cr/")) retVal = "ERR_KICKASS_DETAILS_URL"
                    else {
                        retVal = KASP queryDetails detailsURL
                    }
                }
        }
        retVal
    }
}
