package net.fluxo.plugins.kas.dbo

import java.util

/**
 * Data object representing one page of results from Kickass.to
 *
 * @author Ronald Kurniawan (viper)
 * @version 0.1, 6/01/15.
 */
class KASPage {

    private val SearchResult = "KAST"

    private var _totalItems: Int = 0

    def TotalItems: Int = _totalItems
    def TotalItems_:(value: Int) { _totalItems = value }

    private var _kastItems: util.ArrayList[KASObject] = new util.ArrayList[KASObject]

    def KASItems: util.ArrayList[KASObject] = _kastItems
    def KASItems_: (value: util.ArrayList[KASObject]) { _kastItems = value }
    def AddItem(item: KASObject) { _kastItems add item }
}
