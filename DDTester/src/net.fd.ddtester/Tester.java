package net.fd.ddtester;

import net.fluxo.plugins.kas.impl.KasImpl;

/**
 * @author Ronal Kurniawan (viper)
 * @version 17/01/15
 */
public class Tester {

	public static void main(String[] args) {
		if (args.length < 1) {
			System.out.println("We need at least 1 parameter!");
			System.out.println("SYNTAX: Tester [search-term] [page-number]");
			return;
		}
		String searchTerm = args[0];
		int pageNo = 1;
		if (args.length > 1) {
			try {
				pageNo = Integer.parseInt(args[1]);
			} catch (NumberFormatException nfe) {
				System.out.println("Invalid page number: " + args[1]);
				return;
			}
		}
		System.out.println("Search term: " + searchTerm);
		System.out.println("Page number: " + pageNo);
		System.out.println("Results:\n");

		KasImpl ks = new KasImpl();
		String response = ks.process(new String[] { "DD", "KAST" , "ST=\"" + searchTerm + "\"", "PG=\"" + pageNo + "\"",
			"CAT=\"ALL\""});
		/*String response = ks.process(new String[] { "DD", "KASTDETAILS",
				"https://kickass.so/the-big-bang-theory-s08e12-hdtv-x264-lol-eztv-t10055614.html"});*/
		System.out.println(response);
	}
}
