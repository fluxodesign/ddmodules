/*
 * TPBImpl.scala
 *
 * Copyright (c) 2014 Ronald Kurniawan. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package net.fluxo.plugins.tpb.impl

import net.fluxo.plugins.tpb.TrTPB
import org.apache.log4j.Level
import org.apache.commons.codec.net.URLCodec
import java.net.URLDecoder
import org.apache.commons.io.FilenameUtils
import net.xeoh.plugins.base.annotations.{PluginImplementation, Capabilities}
import net.xeoh.plugins.base.annotations.events.PluginLoaded
import net.xeoh.plugins.base.annotations.meta.Author

/**
 * This is the implementation of TrTPB, the TPB request processing trait (interface).
 * Part of DownloadDaemon plugin framework.
 *
 * @author Ronald Kurniawan (viper)
 * @version 0.1, 30/05/14.
 */
@PluginImplementation
@Author(name="Ronald Kurniawan (viper)")
class TPBImpl extends TrTPB {

	private val _command = "TPB"

	override def primaryCommand(): String = {
		_command
	}

	@Capabilities
	def capabilities(): Array[String] = {
		Array("targetSite:TPB")
	}

	@PluginLoaded
	def pluginDetected() {
		writeToLog("Plugin loaded: PluginTPB", Level.INFO)
	}

	override def setMailLoggerName(name: String) {
		TPBP setMailLogger name
	}

	override def writeToLog(entry: String, logLevel: Level) {
		TPBP writeLog(entry, logLevel)
	}

	override def process(fullCommand: Array[String]): String = {
		// at the very least we need search term...
		// page and categories can also be added
		// syntax: DD TPB ST=[Search Term] PG=[page starting from 0] CAT=[comma-separated xxx code]	
		fullCommand(1) match {
			case "TPB" =>
				if (fullCommand.length < 3) "ERR LENGTH"
				else {
					if (fullCommand.length == 3 && !fullCommand(2).startsWith("ST=")) "SYNTAX ERROR 1"
					else if (fullCommand.length == 4 && (!fullCommand(2).startsWith("ST=") || !fullCommand(3).startsWith("PG="))) "SYNTAX ERROR 2 "
					else if (fullCommand.length >= 5 && (!fullCommand(2).startsWith("ST=") || !fullCommand(3).startsWith("PG=") || !fullCommand(4).startsWith("CAT="))) "SYNTAX ERROR 3"
					else {
						val searchTerm: String = {
							val ucodec = new URLCodec
							ucodec decode fullCommand(2).substring("ST=".length) replaceAllLiterally("\"", "")
						}
						val page: Int = {
							if (fullCommand.length >= 4) {
								try {
									fullCommand(3).substring("PG=".length).toInt
								} catch {
									case nfe: NumberFormatException => 0
								}
							} else 0
						}
						val cat: Array[Int] = {
							if (fullCommand.length >= 5) {
								val cats = fullCommand(4).substring("CAT=".length).split(",")
								val c = new Array[Int](cats.length)
								var counter: Int = 0
								for (x <- cats) {
									c(counter) = x.toInt
									counter += 1
								}
								c
							} else Array[Int]()
						}

						TPBP query(searchTerm, page, cat)
					}
				}
			case "TPBDETAILS" =>
				if (fullCommand.length != 3) "ERR TPBDETAILS SYNTAX"
				else {
					val detailsURL = URLDecoder decode(fullCommand(2), "UTF-8")
					if (!(detailsURL startsWith "http://thepiratebay.se/")) "ERR TPBDETAILS URL"
					else {
						TPBP queryDetails (FilenameUtils getPath detailsURL)
					}
				}
		}
	}
}
