/*
 * DbManager.scala
 *
 * Copyright (c) 2014 Ronald Kurniawan. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package net.fluxo.sc;

import java.sql.*;
import java.security.MessageDigest;

/**
 * DbManager deals with database-related calls.
 *
 * @author Ronald Kurniawan (viper)
 * @version 0.4.5, 23/05/14.
 */
public class DbManager {

	protected static String _connString = "jdbc:h2:dddb;AUTO_RECONNECT=TRUE;LOCK_MODE=1";
	private static Connection _conn = null;

	/**
	 * Set up the database connection.
	 */
	public static void setup() {
		try {
			Class.forName("org.h2.Driver");
			_conn = DriverManager.getConnection(_connString, "devel", "passwd");
		} catch (ClassNotFoundException | SQLException cnfe) {
			System.out.println("Error opening database: " + cnfe.getMessage());
		}
	}

	/**
	 * Create the credentials table if one does not exist in the database.
	 */
	public static void createCredentialsTable() {
		String createStatement = "CREATE TABLE IF NOT EXISTS CREDS(USERNAME VARCHAR(255) NOT NULL DEFAULT 'user'," +
				"PASSWORD VARCHAR(255) NOT NULL DEFAULT 'pass')";
		try {
			PreparedStatement ps = _conn.prepareStatement(createStatement);
			ps.executeUpdate();
			ps.close();
		} catch (SQLException sqle) {
			System.out.println("Error creating credentials table: " + sqle.getMessage());
		}
	}

	/**
	 * Insert credentials into the table. If username already exists, the password will be replaced with the new one.
	 * Password is hashed (MD5).
	 *
	 * @param username username that will be used by client to initiate action (downloading, status, etc.)
	 * @param password original password (not hashed)
	 */
	public static void insertCredentials(String username, String password) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(password.getBytes());
			byte[] digested = md.digest();
			StringBuilder sb = new StringBuilder();
			for (byte aDigested : digested) {
				String hex = Integer.toHexString(0xff & aDigested);
				if ((hex.length()) == 1) {
					sb.append('0');
				}
				sb.append(hex);
			}
			String hashed = sb.toString();
			boolean usernameExists = isUsernameExist(username);
			String updateStatement = "INSERT INTO CREDS(USERNAME,PASSWORD) VALUES(?,?)";
			if (usernameExists) {
				updateStatement = "UPDATE CREDS SET PASSWORD = ? WHERE USERNAME = ?";
			}
			PreparedStatement ps = _conn.prepareStatement(updateStatement);
			if (usernameExists) {
				ps.setString(1, hashed);
				ps.setString(2, username);
			} else {
				ps.setString(1, username);
				ps.setString(2, hashed);
			}
			ps.executeUpdate();
			ps.close();
			System.out.println("Credentials updated.");
		} catch (Exception e) {
			System.out.println("Error updating credentials: " + e.getMessage());
		}
	}

	/**
	 * Check whether the username already exists on the database.
	 *
	 * @param username the username that needs to be checked
	 * @return true if the username already exists; false otherwise
	 */
	private static boolean isUsernameExist(String username) {
		boolean status = false;
		String queryStatement = "SELECT COUNT(*) AS count FROM CREDS WHERE username = ?";
		try {
			PreparedStatement ps = _conn.prepareStatement(queryStatement);
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				if (rs.getInt("count") > 0) {
					status = true;
				}
			}
			rs.close();
			ps.close();
		} catch (SQLException sqle) {
			System.out.println("Error trying to determine if username exists: " + sqle.getMessage());
		}
		return status;
	}

	/**
	 * Close the database connection.
	 */
	public static void cleanup() {
		if (_conn != null) {
			try {
				_conn.close();
			} catch (SQLException sqle) {
				System.out.println("Error trying to close db connection: " + sqle.getMessage());
			}
		}
	}
}