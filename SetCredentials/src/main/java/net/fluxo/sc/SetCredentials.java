/*
 * SetCredentials.java
 *
 * Copyright (c) 2014 Ronald Kurniawan. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package net.fluxo.sc;

import java.io.File;

/**
 * This class process the username and password and tries to set up the credentials
 * table (if none is found) and then save the credentials.
 *
 * @author Ronald Kurniawan (viper)
 * @version 0.4.5, 23/05/14.
 */
class SetCredentials {

	/**
	 * The entry point of the application.
	 * @param args string parameters
	 */
	public static void main(String[] args) {
		if (args.length != 2 || args[0].length() == 0 || args[1].length() == 0) {
			System.out.println("Wrong parameters, or parameter length insufficient.");
			return;
		}
		File dbFile = new File("dddb.h2.db");
		if (!dbFile.exists()) {
			System.out.println("Database file does not exist.");
			return;
		}
		DbManager.setup();
		DbManager.createCredentialsTable();
		DbManager.insertCredentials(args[0], args[1]);
		DbManager.cleanup();
	}
}